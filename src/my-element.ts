import {LitElement, html, customElement, property, css} from 'lit-element';

/**
 * The alert component for regas
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement('regas-alert')
export class AlertElement extends LitElement {


  static styles = css`
    * { 
      margin: 0;
      padding: 0;
      box-sizing: border-box;
      font-family: Arial;
    }
    div {
      position: relative;
      padding: 0.75rem 1.25rem;
      margin-bottom: 1rem;
      border: 2px dashed transparent;
      border-radius: 3px;
      display: flex;
      flex-direction: column;
    }
    .danger {
      color: #f63732;
      fill: #f63732;
      background-color: #fdd7d6;
      border-color: #fa9794;      
    }
    .info {
      color: #28a1cc;
      fill: #28a1cc;
      background-color: #d4ecf5;
      border-color: #8fcee4; 
    }
    .success {
      color: #4aaa50;
      fill: #4aaa50;
      background-color: #dbeedc;
      border-color: #a1d3a4;
    }
    .close {
      align-self: flex-end;
      margin: 0;
      padding: 0;
      background-color: transparent;
      border: none;
    }
    svg {
      transform: rotate(45deg);
      cursor: pointer;
      height: 1.25rem;
      width: 1.25rem;
    }
  `;

  /**
   * wether or not the element is dismissible
   */
  @property({type: Boolean})
  dismissible = false;

  @property({type: String})
  accent = "info"

  private onCloseClick(_event: MouseEvent): void {
    const event = new CustomEvent("close", {
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(event);
  }

  render() {
    const closeButton = this.dismissible ? html`<button class="close" @click="${this.onCloseClick}"><svg>
      <path d="M13.9285714,6.42857143 L8.57142857,6.42857143 L8.57142857,1.07142857 C8.57142857,0.48 8.09142857,0 7.5,0 C6.90857143,0 6.42857143,0.48 6.42857143,1.07142857 L6.42857143,6.42857143 L1.07142857,6.42857143 C0.48,6.42857143 0,6.90857143 0,7.5 C0,8.09142857 0.48,8.57142857 1.07142857,8.57142857 L6.42857143,8.57142857 L6.42857143,13.9285714 C6.42857143,14.52 6.90857143,15 7.5,15 C8.09142857,15 8.57142857,14.52 8.57142857,13.9285714 L8.57142857,8.57142857 L13.9285714,8.57142857 C14.52,8.57142857 15,8.09142857 15,7.5 C15,6.90857143 14.52,6.42857143 13.9285714,6.42857143 Z"/>
    </svg></button>` : undefined;

    const headerDisplay = !!this.parentNode?.querySelector('[slot="header"]');
    const header = headerDisplay ? html`<h3><slot name="header"></slot></h3>` : undefined;

    
    return html`
      <div class="${this.accent}">
        ${closeButton}
        ${header}
        <p><slot></slot></p>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'regas-alert': AlertElement;
  }
}
